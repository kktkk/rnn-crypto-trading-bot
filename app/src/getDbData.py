import pandas as pd
import numpy as np
from models.controller import db
from models.FinancialData import FinancialDataUSD
from flask import Flask

app = Flask(__name__)
app.config.from_object('config.default')
db.init_app(app)

def arrayConstruct():
    with app.app_context():
        dbdata = FinancialDataUSD.query
        dt = pd.read_sql_table(
            'financial_data_usd', 
            'sqlite:///dev.db', 
            parse_dates={'timestamp':'s'}, 
            index_col='timestamp'
            )
        return dt


def resampleOHLC(timeframe):
    data = arrayConstruct()
    dt_ask = data['ask'].resample(timeframe).ohlc()
    dt_bid = data['bid'].resample(timeframe).ohlc()
    dt = pd.concat([dt_ask, dt_bid], axis=0)
    return dt


def resampleOHLC_lastPrice(timeframe):
    data = arrayConstruct()
    dt = data['last_price'].resample(timeframe).ohlc()
    return dt

#D Dummy function for standardization of methods
def getData(timeframe):
    return resampleOHLC(timeframe)
