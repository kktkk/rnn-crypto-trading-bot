import hmac
import time
import hashlib


def encrypt(string):
    return hmac.new(key, string).hexdigest()

def encryptHard(string):
    return hmac.new(key, string, digestmod=hashlib.sha512).hexdigest()

def encryptCompare(string, hash):
    rsl = hmac.compare_digest(
        hmac.new(key, string).hexdigest(),
        str(hash)
    )
    return rsl

def encryptCompareHard(string, hash):
    rsl = hmac.compare_digest(
        hmac.new(key, string, digestmod=hashlib.sha512).hexdigest(),
        str(hash)
    )
    return rsl