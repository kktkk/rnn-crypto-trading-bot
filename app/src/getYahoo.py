import pandas as pd
from pandas_datareader import data as pdr

# Fetch yahoo data
def getData(start="2000-01-01", end="2019-01-26", symbol="BTC-USD"):
    data = pdr.get_data_yahoo(symbol, start=start, end=end) 
    data = pd.DataFrame(data)
    return data