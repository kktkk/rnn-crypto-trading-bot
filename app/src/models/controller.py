#! /bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


# Database Init
db = SQLAlchemy()

# Contenxt handlers for cli tooling
def create_app():
    from models.FinancialData import FinancialDataUSD
    app = Flask(__name__)
    db.init_app(app)
    return app

def save(obj):
    db.session.add(obj)
    db.session.commit()
    return obj.id

def delete(obj):
    db.session.delete(obj)
    db.session.commit()