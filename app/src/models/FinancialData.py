from controller import db, save, delete

# USD pair only!!!!
class FinancialDataUSD(db.Model):
    __tablename__ = 'financial_data_usd'
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    coin = db.Column(db.String(255))
    currency = db.Column(db.String(255))
    high = db.Column(db.Float)
    low = db.Column(db.Float)
    bid = db.Column(db.Float)
    ask = db.Column(db.Float)
    last_price = db.Column(db.Float)
    volume = db.Column(db.Float)
    timestamp = db.Column(db.Float)

    def save(self):
        save(self)