# Moving Averages Code

# Load the necessary packages and modules
from getYahoo import getData as getYahoo
from getDbData import getData as getDb
import matplotlib.pyplot as plt
import fix_yahoo_finance
import pandas as pd

# Simple Moving Average 
def SMA(data, period, col): 
    SMA = pd.Series(col.rolling(period).mean(), name = 'SMA') 
    data = data.join(SMA) 
    return data

# Exponentially-weighted Moving Average 
def EWMA(data, period, col):
    EMA = pd.Series(col.ewm(span=period, min_periods=period-1).mean(), name = 'EWMA_' + str(period))
    data = data.join(EMA) 
    return data

# Visualization of Moving Averages
def visualize(data, col, period_sma=50, period_ewma=200):
    sma = SMA(data,period_sma, col)
    sma = sma.dropna()
    SimpleMovingAverage = sma['SMA']
    ewma = EWMA(data,period_ewma, col)
    ewma = ewma.dropna()
    ExponentialWeightedMovingAverage = ewma['EWMA_200']

    # Plotting the Price Series chart and Moving Averages below
    plt.figure(figsize=(9,5))
    plt.plot(col,lw=1, label='NSE Prices')
    plt.plot(SimpleMovingAverage,'g',lw=1, label='50-day SMA (green)')
    plt.plot(ExponentialWeightedMovingAverage,'r', lw=1, label='200-day EWMA (red)')
    plt.legend(loc=2,prop={'size':11})
    plt.grid(True)
    plt.setp(plt.gca().get_xticklabels(), rotation=30)
    plt.show()


data = getYahoo()
visualize(data, data['Close'])