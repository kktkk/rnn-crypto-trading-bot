import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# from matplotlib.finance import candlestick_ohlc
from mpl_finance import candlestick_ohlc
from getDbData import resampleOHLC


def volatility(timeframe, period):
    data = resampleOHLC(timeframe)
    # Compute the logarithmic returns using the Closing price
    data['Log_Ret'] = np.log(data['close'] / data['close'].shift(1))
    # Compute Volatility using the pandas rolling standard deviation function
    data['Volatility'] = data['Log_Ret'].rolling(period).std() * np.sqrt(period)
    # print data['Volatility']
    return data['Volatility']


# Sharpe Ratio 
def sharpe(returns, risk_free_rate, period):
    volatility = returns.std() * np.sqrt(period) 
    sharpe_ratio = (returns.mean() - risk_free_rate) / volatility
    return sharpe_ratio


def showDataSimple():
    dbdata = ''
    timestamps = []
    last_price = []
    high = []
    low = []
    for i in dbdata:
        timestamps.append(i.timestamp)
        last_price.append(i.last_price)
        high.append(i.high)
        low.append(i.low)
    plt.plot(timestamps, last_price)
    plt.xlabel('timestamps')
    plt.ylabel('last price')
    plt.show()


def showVolatility(timeframe, period):
    data = resampleOHLC(timeframe)
    volat = volatility(timeframe, period)
    full_data = pd.concat([data, volat], axis=1)
    print full_data
    full_data[['close', 'Volatility']].plot(subplots=True, color='blue',figsize=(8, 6))
    plt.show()

if __name__ == '__main__':
    # showData() 
    showVolatility("5min", 2)