HOST = '0.0.0.0'
PORT = 9999

SQLALCHEMY_DATABASE_URI = 'sqlite:///dev.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = False

SECRET_KEY = 'dummy secret'
SECURITY_PASSWORD_SALT = 'blabla'

TAGS_FORBIDEN = ['<', '>', '"', '"']

# LOGS_DIR = 'logs/'
CORS_ORIGIN = '*'
EMAIL_CONFIRM_EXPIRATION = 3600 # in seconds

DEBUG = True

# Mail config
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True
MAIL_USERNAME =  'staking.store@gmail.com'
MAIL_PASSWORD = 'Paranoia0'
MAIL_DEFAULT_SENDER = 'no-reply@staking.store'
MAIL_MAX_EMAILS = False
MAIL_ASCII_ATTACHMENTS = False
MAIL_ADMIN = 'staking.store@gmail.com'