import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from getDbData import getData as getDb
from getYahoo import getData as getYahoo

def strategy_performance(data, col, timeframe, period, *symbol):
    if not data:
        data = pdr.get_data_yahoo(symbol, start=start, end=end) 
    data['high'] = col.shift(1).rolling(period).max()
    data['low'] = col.shift(1).rolling(period).min()
    data['avg'] = col.shift(1).rolling(period).mean()
    # Entry Rules
    data['long_entry'] = col < data.avg
    data['short_entry'] = col > data.avg
    # Exit rules
    data['long_exit'] = col > data.avg
    data['short_exit'] = col < data.avg

    data['positions_long'] = np.nan
    data.loc[data.long_entry, 'positions_long'] = 1
    data.loc[data.long_exit, 'positions_long'] = 0

    data['positions_short'] = np.nan
    data.loc[data.short_entry, 'positions_short'] = 1
    data.loc[data.short_exit, 'positions_short'] = 0

    data['signal'] = data.positions_long + data.positions_short

    data = data.fillna(method='ffill')
    print data
    daily_log_returns = np.log(col / col.shift(1))
    daily_log_returns = daily_log_returns * data.signal.shift(1)
    return daily_log_returns

def cumulative_sum(data, col, timeframe, period, symbol):
    if symbol:
        dat = strategy_performance(data, col, timeframe, period, symbol)
    dat = strategy_performance(data, col, timeframe, period)
    return dat.cumsum()

def visualize_histogram(data, col, timeframe, period):
    dat = strategy_performance(data, timeframe, period)
    dat.hist()
    plt.show()

def visualize_perf_single(data, col, timeframe, period):
    dat = cumulative_sum(data, col, timeframe, period)
    dt = pd.DataFrame(dat)
    dt.plot()
    plt.show()

def visualize_perf_multi(data, col, timeframe, period, portfolio):
    cum_d_ret = pd.DataFrame()
    for i in portfolio:
        cum_d_ret[i] = cumulative_sum(data, col, timeframe, period, i)
    cum_d_ret.plot()
    plt.show()

portfolio
data = getYahoo()
visualize_perf_single(data, data['Close'], '5min', 10)