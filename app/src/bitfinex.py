# -*- coding: utf-8 -*-
import time
import logging
import requests as r
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from models.controller import db
from models.FinancialData import FinancialDataUSD
from flask_migrate import Migrate

app = Flask(__name__)
#pp.config.from_object('config.default')
app.config.from_pyfile('config.py')

## DATABASE
db.init_app(app)
migrate = Migrate(app, db)

## LOGIC
# Get data from platform
apiUrl = 'https://api.bitfinex.com/v1/'
last_heartbeat = int(time.time())

## Get ticker Data
def getTickerData(pair):
    endpoint = 'pubticker/'
    url = apiUrl + endpoint + pair + 'usd'
    req = r.get(url)
    last_heartbeat = int(time.time())
    return json.loads(req.text)
    
    
#Save data to db
def saveData2Db(pair):
    webdata = getTickerData(pair)
    print(webdata)
    dbData = FinancialDataUSD.query.filter_by(timestamp=webdata['timestamp']).first()
    # check id data exists
    if dbData:
        msg = 'data already exists!'
        logging.info(msg)
        return False
    # if not save data
    data = FinancialDataUSD(
        coin=pair,
        currency='usd',
        bid = webdata['bid'],
        ask = webdata['ask'],
        last_price = webdata['last_price'],
        low = webdata['low'],
        high = webdata['high'],
        volume = webdata['volume'],
        timestamp = webdata['timestamp']
    )
    # return id of saved data or null
    FinancialDataUSD.save(data)
    return True


def run(pair):
    # Maximum 20 requests per minute!
    while True:
        saveData2Db(pair)
        time.sleep(6)


if __name__ == '__main__':
    with app.app_context():
        run('btc')
